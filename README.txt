README
------
Read the INSTALL.txt file for installation directions.

PLEASE note that at the moment, this module is NOT compatible with other 
authentication modules that you may be using.  It will only work for users
stored with Drupal's user table.  

ALSO note that merely enabling the module means that the javascript will be
active wherever you have a login box displayed.  Users will still be able to
log in if they have javascript disabled, but of course their password will be
sent in the clear.  I plan on making the fact that the javascript is running
more obvious in the future, along with better controls for visibility.

ABOUT
-----
CRAM is a Drupal module that implements the Challenge-Response
Authentication Mechanism for logging in.  I developed it because I hated
logging into my Drupal installation when I knew my password was being sent
clear over the wire.  The company hosting my site did not offer SSL (or at
least not at a cheap price), so I looked at other options and came up with
this.

CONTACT
-------
Email me at Joe Selman <idlehero at gmail.com> if you have any
questions/comments.
