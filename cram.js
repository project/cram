if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $("#user-login-form").submit(cramHashPass);
    $("#user-login").submit(cramHashPass);
  });

  function cramHashPass() {
    var $form;
    if ( $("#user-login-form").length) {
      $form = $("#user-login-form");
    } else {
      $form = $("#user-login");
    }
    var passField = $("#edit-pass").get(0);
    var nonce = $("#edit-cram-nonce").get(0);
    if (passField.value != '') {
      passField.value = hex_hmac_md5(hex_md5(passField.value), nonce.value);
    }

    $form.unbind();
    $form.get(0).submit();
    $("#edit-submit").attr("disabled", "disabled");
    return false;
  }
}
